/*Nomor 1*/
select nm_film, nominasi from film order by nominasi asc ;
/*Nomor 2*/
select nm_film, nominasi from film order by nominasi desc limit 2;
/*Nomor 3*/
select nm_film, nominasi from film where nominasi = 0;
/*Nomor 4*/
select nm_film, pendapatan from film order by pendapatan asc;
/*Nomor 5*/
select nm_film, max(pendapatan) from film;
/*Nomor 6*/
select nm_film from film where nm_film like 'p%';
/*Nomor 7*/
select nm_film from film where nm_film like '%h';
/*Nomor 8*/
select nm_film from film where nm_film like '%d%';
/*Nomor 9*/
select nm_film, max(pendapatan) from film where nm_film like '%o%';
/*Nomor 10*/
select nm_film, min(pendapatan) from film where nm_film like '%o%';
/*Nomor 11*/
select a.nm_film, b.nm_artis from
	film as a 
join artis as b 
	on a.artis = b.kd_artis;
/*Nomor 12*/
select a.nm_film, b.nm_artis from
	 film as a 
join artis as b 
	on a.artis = b.kd_artis where b.negara = 'HK'; 
/*Nomor 13*/
select film.nm_film, artis.nm_artis, negara.nm_negara from film
join artis on film.artis = artis.kd_artis
join negara on artis.negara = negara.kd_negara
where negara.nm_negara not like '%o%'; 	
/*Nomor 14 not fix*/
select artis.nm_artis from film 
left join artis on film.artis = artis.kd_artis
where artis.kd_artis is null; 
/*Nomor 15*/
select artis.nm_artis from film 
join artis on film.artis = artis.kd_artis
join genre on film.genre = genre.kd_genre
where genre.nm_genre = 'DRAMA';
/*Nomor 16*/
select artis.nm_artis from film 
join artis on film.artis = artis.kd_artis
join genre on film.genre = genre.kd_genre
where genre.nm_genre = 'HORROR';
/*Nomor 17 not fix*/
select negara.nm_negara, count(film.artis) as jumlah_film from negara
join artis on negara.kd_negara = artis.negara
join film on artis.kd_artis = film.artis;
/*Nomor 18*/
select nm_produser from produser where international = 'YA';
/*Nomor 19 not fix*/
select count(film.kd_film) as jumlah_film , produser.nm_produser from film
left join produser on film.produser = produser.kd_produser;
/*Nomor 20*/
select sum(film.pendapatan) as jumlah , produser.nm_produser from film
left join produser on film.produser = produser.kd_produser
where produser.nm_produser like 'MARVEL';
