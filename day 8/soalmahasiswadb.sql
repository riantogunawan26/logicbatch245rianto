/*Nomor 1*/
CREATE DATABASE `dbuniversitas`;

CREATE TABLE `jurusankuliah` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KodeJurusan` varchar(12) NOT NULL,
  `Deskripsi` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
);

INSERT INTO `jurusankuliah` (`ID`, `KodeJurusan`, `Deskripsi`) VALUES
	(1, 'KA001', 'Teknik Informatika'),
	(2, 'KA002', 'Management Bisnis'),
	(3, 'KA003', 'Ilmu Komunikasi'),
	(4, 'KA004', 'Sastra Inggris'),
	(5, 'KA005', 'Ilmu Pengetahuan Alam Dan Matematika'),
	(6, 'KA006', 'Kedokteran');
	
CREATE TABLE `dosen` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KodeDosen` varchar(14) NOT NULL,
  `NamaDosen` varchar(15) NOT NULL,
  `KodeMataKuliah` varchar(12) NOT NULL,
  `KodeFakultas` varchar(13) NOT NULL,
  PRIMARY KEY (`ID`)
);

INSERT INTO `dosen` (`ID`, `KodeDosen`, `NamaDosen`, `KodeMataKuliah`, `KodeFakultas`) VALUES
	(1, 'GK001', 'Ahmad Prasetyo', 'KPK001', 'TK002'),
	(2, 'GK002', 'Hadi Fuladi', 'KPK002', 'TK001'),
	(3, 'GK003', 'Johan Goerge', 'KPK003', 'TK002'),
	(4, 'GK004', 'Bima Darmawan', 'KPK004', 'TK002'),
	(5, 'GK005', 'Gatot Wahyudi', 'KPK005', 'TK001');
	
CREATE TABLE `matakuliah` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KodeMataKuliah` varchar(12) NOT NULL,
  `NamaMataKuliah` varchar(50) NOT NULL,
  `KeaktifanStatus` varchar(15) NOT NULL,
  PRIMARY KEY (`ID`)
);

INSERT INTO `matakuliah` (`ID`, `KodeMataKuliah`, `NamaMataKuliah`, `KeaktifanStatus`) VALUES
	(1, 'KPK001', 'Algoritma Dasar', 'Aktif'),
	(2, 'KPK002', 'Basis Data', 'Aktif'),
	(3, 'KPK003', 'Kalkulus', 'Aktif'),
	(4, 'KPK004', 'Pengantar Bisnis', 'Aktif'),
	(5, 'KPK005', 'Matematika Ekonomi & Bisnis', 'Non Aktif');

CREATE TABLE `mahasiswa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KodeMahasiswa` varchar(12) NOT NULL,
  `NamaMahasiswa` varchar(20) NOT NULL,
  `AlamatMahasiswa` varchar(30) NOT NULL,
  `KodeJurusan` varchar(12) NOT NULL,
  `KodeMataKuliah` varchar(12) NOT NULL,
  PRIMARY KEY (`ID`)
);

INSERT INTO `mahasiswa` (`ID`, `KodeMahasiswa`, `NamaMahasiswa`, `AlamatMahasiswa`, `KodeJurusan`, `KodeMataKuliah`) VALUES
	(1, 'MK001', 'Fullan', 'Jl. Hati besar no 63 RT.13', 'KA001', 'KPK001'),
	(2, 'MK002', 'Fullana Binharjo', 'Jl. Biak kebagusan No. 34', 'KA002', 'KPK002'),
	(3, 'MK003', 'Sardine Himura', 'Jl. Kebajian no 84', 'KA001', 'KPK003'),
	(4, 'MK004', 'Isani isabul', 'Jl. Merak merpati No.78', 'KA001', 'KPK001'),
	(5, 'MK005', 'Charlie birawa', 'Jl. Air terjun semidi No.56', 'KA003', 'KPK002');

CREATE TABLE `nilaimahasiswa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KodeNilai` varchar(12) NOT NULL,
  `KodeMahasiswa` varchar(12) NOT NULL,
  `KodeOrganisasi` varchar(9) NOT NULL,
  `Nilai` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
);

INSERT INTO `nilaimahasiswa` (`ID`, `KodeNilai`, `KodeMahasiswa`, `KodeOrganisasi`, `Nilai`) VALUES
	(1, 'SK001', 'MK004', 'KK001', 90),
	(2, 'SK002', 'MK001', 'KK001', 80),
	(3, 'SK003', 'MK002', 'KK003', 85),
	(4, 'SK004', 'MK004', 'KK002', 95),
	(5, 'SK005', 'MK005', 'KK005', 70);
	
CREATE TABLE IF NOT EXISTS `fakultas` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KodeFakultas` varchar(13) NOT NULL,
  `Penjelasan` varchar(30) NOT NULL,
  PRIMARY KEY (`ID`)
);

INSERT INTO `fakultas` (`ID`, `KodeFakultas`, `Penjelasan`) VALUES
	(1, 'TK001', 'Teknik Informatika'),
	(2, 'TK002', 'Matematika'),
	(3, 'TK003', 'Sistem Informatika');
	
CREATE TABLE `organisasi` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KodeOrganisasi` varchar(12) NOT NULL,
  `NamaOrganisasi` varchar(100) NOT NULL,
  `StatusOrganisasi` varchar(13) NOT NULL,
  PRIMARY KEY (`ID`)
);

INSERT INTO `organisasi` (`ID`, `KodeOrganisasi`, `NamaOrganisasi`, `StatusOrganisasi`) VALUES
	(1, 'KK001', 'Unit Kegiatan Mahasiswa (UKM)', 'Aktif'),
	(2, 'KK002', 'Badan Eksekutif Mahasiswa Fakultas (BEMF)', 'Aktif'),
	(3, 'KK003', 'Dewan Perwakilan Mahasiswa Universitas (DPMU)', 'Aktif'),
	(4, 'KK004', 'Badan Eksekutif Mahasiswa Universitas (BEMU)', 'Non Aktif'),
	(5, 'KK005', 'Himpunan Mahasiswa Jurusan', 'Non Aktif'),
	(6, 'KK006', 'Himpunan Kompetisi Jurusan', 'Aktif');

/*Nomor 2*/
ALTER TABLE `dosen`
	CHANGE COLUMN `NamaDosen` `NamaDosen` VARCHAR(200) NOT NULL AFTER `KodeDosen`;
/*Nomor 3*/
SELECT mahasiswa.KodeMahasiswa, mahasiswa.NamaMahasiswa,
		 matakuliah.NamaMataKuliah, jurusankuliah.Deskripsi
FROM mahasiswa 
JOIN matakuliah ON mahasiswa.KodeMataKuliah = matakuliah.KodeMataKuliah
JOIN jurusankuliah ON mahasiswa.KodeJurusan = jurusankuliah.KodeJurusan
WHERE mahasiswa.KodeMahasiswa = 'MK001';
/*Nomor 4*/
SELECT matakuliah.NamaMataKuliah, matakuliah.KeaktifanStatus,
		mahasiswa.KodeMahasiswa, mahasiswa.NamaMahasiswa,
		mahasiswa.KodeJurusan, mahasiswa.KodeMataKuliah
FROM mahasiswa 
JOIN matakuliah ON mahasiswa.KodeMataKuliah = matakuliah.KodeMataKuliah
JOIN jurusankuliah ON mahasiswa.KodeJurusan = jurusankuliah.KodeJurusan
WHERE matakuliah.KeaktifanStatus = 'Non Aktif';
/*Nomor 5*/
SELECT nilaimahasiswa.KodeMahasiswa, mahasiswa.NamaMahasiswa,
		mahasiswa.AlamatMahasiswa
FROM nilaimahasiswa
JOIN mahasiswa ON nilaimahasiswa.KodeMahasiswa = mahasiswa.KodeMahasiswa
JOIN organisasi ON nilaimahasiswa.KodeOrganisasi = organisasi.KodeOrganisasi
WHERE organisasi.StatusOrganisasi = 'Aktif' AND nilaimahasiswa.Nilai > 79; 
/*Nomor 6*/
SELECT * FROM matakuliah
WHERE NamaMataKuliah like '%n%';
/*Nomor 7*/
SELECT mahasiswa.NamaMahasiswa FROM mahasiswa
RIGHT JOIN nilaimahasiswa ON mahasiswa.KodeMahasiswa = nilaimahasiswa.KodeMahasiswa
GROUP BY nilaimahasiswa.KodeMahasiswa HAVING COUNT(nilaimahasiswa.KodeMahasiswa) > 1;
/*Nomor 8*/
SELECT mahasiswa.KodeMahasiswa, mahasiswa.NamaMahasiswa,
		 matakuliah.NamaMataKuliah, jurusankuliah.Deskripsi,
		 dosen.NamaDosen, matakuliah.KeaktifanStatus,
		 fakultas.Penjelasan
FROM mahasiswa 
JOIN matakuliah ON mahasiswa.KodeMataKuliah = matakuliah.KodeMataKuliah
JOIN jurusankuliah ON mahasiswa.KodeJurusan = jurusankuliah.KodeJurusan
JOIN dosen ON mahasiswa.KodeMataKuliah = dosen.KodeMataKuliah
JOIN fakultas ON dosen.KodeFakultas = fakultas.KodeFakultas
WHERE mahasiswa.KodeMahasiswa = 'MK001';
/*Nomor 9*/
CREATE VIEW `vw_universitas` 
AS SELECT mahasiswa.KodeMahasiswa, mahasiswa.NamaMahasiswa, matakuliah.NamaMataKuliah,
			 dosen.NamaDosen, matakuliah.KeaktifanStatus, fakultas.Penjelasan
FROM mahasiswa 
JOIN matakuliah ON mahasiswa.KodeMataKuliah = matakuliah.KodeMataKuliah
JOIN jurusankuliah ON mahasiswa.KodeJurusan = jurusankuliah.KodeJurusan
JOIN dosen ON mahasiswa.KodeMataKuliah = dosen.KodeMataKuliah
JOIN fakultas ON dosen.KodeFakultas = fakultas.KodeFakultas;
/*Nomor 10*/
SELECT * FROM vw_universitas
WHERE KodeMahasiswa = 'MK001';
