-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.9-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for db_entertainment
DROP DATABASE IF EXISTS `db_entertainment`;
CREATE DATABASE IF NOT EXISTS `db_entertainment` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_entertainment`;


-- Dumping structure for table db_entertainment.artis
DROP TABLE IF EXISTS `artis`;
CREATE TABLE IF NOT EXISTS `artis` (
  `kd_artis` varchar(100) NOT NULL,
  `nm_artis` varchar(100) NOT NULL,
  `jk` varchar(100) NOT NULL,
  `bayaran` int(11) NOT NULL,
  `award` int(11) NOT NULL,
  `negara` varchar(100) NOT NULL,
  PRIMARY KEY (`kd_artis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_entertainment.artis: ~5 rows (approximately)
DELETE FROM `artis`;
/*!40000 ALTER TABLE `artis` DISABLE KEYS */;
INSERT INTO `artis` (`kd_artis`, `nm_artis`, `jk`, `bayaran`, `award`, `negara`) VALUES
	('A001', 'ROBERT DOWNEY JR', 'PRIA', 500000000, 2, 'AS'),
	('A002', 'ANGELINA JOLIE', 'WANITA', 700000000, 1, 'AS'),
	('A003', 'JACKIE CHAN', 'PRIA', 200000000, 7, 'HK'),
	('A004', 'JOE TASLIM', 'PRIA', 350000000, 1, 'ID'),
	('A005', 'CHELSEA ISLAN', 'WANITA', 300000000, 0, 'ID');
/*!40000 ALTER TABLE `artis` ENABLE KEYS */;


-- Dumping structure for table db_entertainment.film
DROP TABLE IF EXISTS `film`;
CREATE TABLE IF NOT EXISTS `film` (
  `kd_film` varchar(10) NOT NULL,
  `nm_film` varchar(55) NOT NULL,
  `genre` varchar(55) NOT NULL,
  `artis` varchar(55) NOT NULL,
  `produser` varchar(55) NOT NULL,
  `pendapatan` int(11) NOT NULL,
  `nominasi` int(11) NOT NULL,
  PRIMARY KEY (`kd_film`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_entertainment.film: ~13 rows (approximately)
DELETE FROM `film`;
/*!40000 ALTER TABLE `film` DISABLE KEYS */;
INSERT INTO `film` (`kd_film`, `nm_film`, `genre`, `artis`, `produser`, `pendapatan`, `nominasi`) VALUES
	('F001', 'IRON MAN', 'G001', 'A001', 'PD01', 2000000000, 3),
	('F002', 'IRON MAN 2', 'G001', 'A001', 'PD01', 1800000000, 2),
	('F003', 'IRON MAN 3', 'G001', 'A001', 'PD01', 1200000000, 0),
	('F004', 'AVENGER : CIVIL WAR', 'G001', 'A001', 'PD01', 2000000000, 1),
	('F005', 'SPIDERMAN HOME COMING', 'G001', 'A001', 'PD01', 1300000000, 0),
	('F006', 'THE RAID', 'G001', 'A004', 'PD03', 800000000, 5),
	('F007', 'FAST & FURIOUS', 'G001', 'A004', 'PD05', 830000000, 2),
	('F008', 'HABIBIE DAN AINUN', 'G004', 'A005', 'PD03', 670000000, 4),
	('F009', 'POLICE STORY', 'G001', 'A003', 'PD02', 700000000, 3),
	('F010', 'POLICE STORY 2', 'G001', 'A003', 'PD02', 710000000, 1),
	('F011', 'POLICE STORY 3', 'G001', 'A003', 'PD02', 615000000, 0),
	('F012', 'RUSH HOUR', 'G003', 'A003', 'PD05', 695000000, 2),
	('F013', 'KUNGFU PANDA', 'G003', 'A003', 'PD05', 923000000, 5);
/*!40000 ALTER TABLE `film` ENABLE KEYS */;


-- Dumping structure for table db_entertainment.genre
DROP TABLE IF EXISTS `genre`;
CREATE TABLE IF NOT EXISTS `genre` (
  `kd_genre` varchar(50) NOT NULL,
  `nm_genre` varchar(50) NOT NULL,
  PRIMARY KEY (`kd_genre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_entertainment.genre: ~6 rows (approximately)
DELETE FROM `genre`;
/*!40000 ALTER TABLE `genre` DISABLE KEYS */;
INSERT INTO `genre` (`kd_genre`, `nm_genre`) VALUES
	('G001', 'ACTION'),
	('G002', 'HORROR'),
	('G003', 'COMEDY'),
	('G004', 'DRAMA'),
	('G005', 'THRILLER'),
	('G006', 'FICTION');
/*!40000 ALTER TABLE `genre` ENABLE KEYS */;


-- Dumping structure for table db_entertainment.negara
DROP TABLE IF EXISTS `negara`;
CREATE TABLE IF NOT EXISTS `negara` (
  `kd_negara` varchar(100) NOT NULL,
  `nm_negara` varchar(100) NOT NULL,
  PRIMARY KEY (`kd_negara`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_entertainment.negara: ~4 rows (approximately)
DELETE FROM `negara`;
/*!40000 ALTER TABLE `negara` DISABLE KEYS */;
INSERT INTO `negara` (`kd_negara`, `nm_negara`) VALUES
	('AS', 'AMERIKA SERIKAT'),
	('HK', 'HONGKONG'),
	('ID', 'INDONESIA'),
	('IN', 'INDIA');
/*!40000 ALTER TABLE `negara` ENABLE KEYS */;


-- Dumping structure for table db_entertainment.produser
DROP TABLE IF EXISTS `produser`;
CREATE TABLE IF NOT EXISTS `produser` (
  `kd_produser` varchar(50) NOT NULL,
  `nm_produser` varchar(50) NOT NULL,
  `international` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`kd_produser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_entertainment.produser: ~5 rows (approximately)
DELETE FROM `produser`;
/*!40000 ALTER TABLE `produser` DISABLE KEYS */;
INSERT INTO `produser` (`kd_produser`, `nm_produser`, `international`) VALUES
	('PD01', 'MARVEL', 'YA'),
	('PD02', 'HONGKONG CINEMA', 'YA'),
	('PD03', 'RAPI FILM', 'TIDAK'),
	('PD04', 'PARKIT', 'TIDAK'),
	('PD05', 'PARAMOUNT PICTURE', 'YA');
/*!40000 ALTER TABLE `produser` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
