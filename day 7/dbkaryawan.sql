-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.9-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for dbkaryawan
DROP DATABASE IF EXISTS `dbkaryawan`;
CREATE DATABASE IF NOT EXISTS `dbkaryawan` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dbkaryawan`;


-- Dumping structure for table dbkaryawan.cuti_karyawan
DROP TABLE IF EXISTS `cuti_karyawan`;
CREATE TABLE IF NOT EXISTS `cuti_karyawan` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomor_induk` varchar(7) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `lama_cuti` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table dbkaryawan.cuti_karyawan: ~0 rows (approximately)
DELETE FROM `cuti_karyawan`;
/*!40000 ALTER TABLE `cuti_karyawan` DISABLE KEYS */;
INSERT INTO `cuti_karyawan` (`id`, `nomor_induk`, `tanggal_mulai`, `lama_cuti`, `keterangan`) VALUES
	(1, 'IP06001', '2012-02-01', 3, 'Acara keluar'),
	(2, 'IP06001', '2012-02-13', 4, 'Anak sakit'),
	(3, 'IP07007', '2012-02-15', 2, 'Nenek sakit'),
	(4, 'IP06003', '2012-02-17', 1, 'Mendaftar sekolah anak'),
	(5, 'IP07006', '2012-02-20', 5, 'Menikah'),
	(6, 'IP07004', '2012-02-27', 1, 'Imunisasi anak');
/*!40000 ALTER TABLE `cuti_karyawan` ENABLE KEYS */;


-- Dumping structure for table dbkaryawan.karyawan
DROP TABLE IF EXISTS `karyawan`;
CREATE TABLE IF NOT EXISTS `karyawan` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomor_induk` varchar(7) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `tanggal_masuk` date NOT NULL,
  PRIMARY KEY (`nomor_induk`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table dbkaryawan.karyawan: ~10 rows (approximately)
DELETE FROM `karyawan`;
/*!40000 ALTER TABLE `karyawan` DISABLE KEYS */;
INSERT INTO `karyawan` (`id`, `nomor_induk`, `nama`, `alamat`, `tanggal_lahir`, `tanggal_masuk`) VALUES
	(1, 'IP06001', 'Agus', 'Jln. Gajah Mada 115A, Jakarta Pusat', '1970-08-01', '2006-07-07'),
	(2, 'IP06002', 'Amin', 'Jln. Bungur sari v No, 178, bandung', '1977-05-03', '2006-07-06'),
	(3, 'IP06003', 'Yusuf', 'Jln. Yosodpuro 15, surabaya', '1973-08-09', '2006-07-08'),
	(4, 'IP07004', 'Alyssa', 'Jln. Cendana No. 6 Bandung', '1983-02-14', '2007-01-05'),
	(5, 'IP07005', 'Maulana', 'Jln. Ampera Raya No 1', '1985-10-10', '2007-02-05'),
	(6, 'IP07006', 'Afika', 'Jln. Pejaten Barat No 6A', '1987-03-09', '2007-06-09'),
	(7, 'IP07007', 'James', 'Jln. Padjadjaran No. 111, bandung', '1988-05-19', '2006-06-09'),
	(8, 'IP09008', 'Octavanus', 'Jln. Gajah Mada 101, Semarang', '1988-10-07', '2008-08-08'),
	(9, 'IP09009', 'Nugroho', 'Jln. Duren Tiga 196, Jakarta Selatan', '1988-01-20', '2008-11-11'),
	(10, 'IP09010', 'Raisa', 'Jln. Nangka Jakarta selatan', '1989-12-29', '2009-02-09');
/*!40000 ALTER TABLE `karyawan` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
