-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.9-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for dbpenerbit
DROP DATABASE IF EXISTS `dbpenerbit`;
CREATE DATABASE IF NOT EXISTS `dbpenerbit` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dbpenerbit`;


-- Dumping structure for table dbpenerbit.tblgaji
DROP TABLE IF EXISTS `tblgaji`;
CREATE TABLE IF NOT EXISTS `tblgaji` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Kd_Pengarang` varchar(7) NOT NULL DEFAULT '0',
  `Nama` varchar(30) NOT NULL DEFAULT '0',
  `Gaji` decimal(18,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table dbpenerbit.tblgaji: ~6 rows (approximately)
DELETE FROM `tblgaji`;
/*!40000 ALTER TABLE `tblgaji` DISABLE KEYS */;
INSERT INTO `tblgaji` (`ID`, `Kd_Pengarang`, `Nama`, `Gaji`) VALUES
	(1, 'P0002', 'Rian', 600000.00),
	(2, 'P0005', 'Amir', 700000.00),
	(3, 'P0004', 'Siti', 500000.00),
	(4, 'P0003', 'Suwadi', 1000000.00),
	(5, 'P0010', 'Fatmawati', 600000.00),
	(6, 'P0008', 'Saman', 750000.00);
/*!40000 ALTER TABLE `tblgaji` ENABLE KEYS */;


-- Dumping structure for table dbpenerbit.tblpengarang
DROP TABLE IF EXISTS `tblpengarang`;
CREATE TABLE IF NOT EXISTS `tblpengarang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Kd_Pengarang` varchar(7) NOT NULL,
  `Nama` varchar(30) NOT NULL,
  `Alamat` varchar(80) NOT NULL,
  `Kota` varchar(15) NOT NULL,
  `Kelamin` varchar(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table dbpenerbit.tblpengarang: ~10 rows (approximately)
DELETE FROM `tblpengarang`;
/*!40000 ALTER TABLE `tblpengarang` DISABLE KEYS */;
INSERT INTO `tblpengarang` (`ID`, `Kd_Pengarang`, `Nama`, `Alamat`, `Kota`, `Kelamin`) VALUES
	(1, 'P0001', 'Ashadi', 'Jl. Beo 25', 'Yogya', 'P'),
	(2, 'P0002', 'Rian', 'Jl. Solo 123', 'Yogya', 'P'),
	(3, 'P0003', 'Suwadi', 'Jl. Semangka 13 ', 'Bandung', 'P'),
	(4, 'P0004', 'Siti', 'Jl. Durian 15 ', 'Solo', 'W'),
	(5, 'P0005', 'Amir', 'Jl. Gajah 33 ', 'Bandung', 'P'),
	(6, 'P0006', 'Suparman', 'Jl. Harimau 25 ', 'Kudus', 'P'),
	(7, 'P0007', 'Jaja', 'Jl. Singa 7', 'Bandung', 'P'),
	(8, 'P0008', 'Saman', 'Jl. Naga 12 ', 'Yogya', 'P'),
	(9, 'P0009', 'Anwar', 'Jl. Tidar 6A ', 'Magelang', 'P'),
	(10, 'P0010', 'Fatmawati', 'Jl. Renjana 4', 'Bogor', 'W');
/*!40000 ALTER TABLE `tblpengarang` ENABLE KEYS */;


-- Dumping structure for view dbpenerbit.vwpengarang
DROP VIEW IF EXISTS `vwpengarang`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `vwpengarang` (
	`Kd_Pengarang` VARCHAR(7) NOT NULL COLLATE 'latin1_swedish_ci',
	`Nama` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci',
	`Kota` VARCHAR(15) NOT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;


-- Dumping structure for view dbpenerbit.vwpengarang
DROP VIEW IF EXISTS `vwpengarang`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `vwpengarang`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `vwpengarang` AS SELECT Kd_Pengarang,Nama,Kota from tblpengarang ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
