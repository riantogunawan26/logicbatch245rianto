/*
Nomor 1
*/
SELECT count(Kd_Pengarang) AS jumlahpengarang FROM tblpengarang
/*
Nomor 2
*/
select count(Kelamin) as Pria from tblpengarang where Kelamin = 'P';
select count(Kelamin) as Wanita from tblpengarang where Kelamin = 'W';
/*
Nomor 3
*/
select count(Kota) as JumlahKota, Kota from tblpengarang group by Kota;
/*
Nomor 4
*/
select count(Kota) as JumlahKota, Kota from tblpengarang group by Kota having count(Kota) > 1;
/*
Nomor 5
*/

/*
Nomor 6
*/
select max(Gaji) as Gaji_Max from tblgaji;
select min(Gaji) as Gaji_Min from tblgaji;
/*
Nomor 7
*/
select Gaji from tblgaji where Gaji > 600000;
/*
Nomor 8
*/
select sum(Gaji) as Jumlah_Gaji from tblgaji;
/*
Nomor 9
*/
select sum(b.Gaji) as Jumlah_Gaji from tblpengarang as a 
left join tblgaji as b on a.Kd_Pengarang = b.Kd_Pengarang where a.Kota = 'Bandung';
/*
Nomor 10
*/
select ID,Kd_Pengarang,Nama,Alamat,Kota,Kelamin 
from tblpengarang where Kd_Pengarang between 'P0001' and 'P0006';
/*
Nomor 11
*/
select kota from tblpengarang where Kota in ('Solo','Magelang','Yogya');
/*
Nomor 12
*/
select * from tblpengarang where Kota not like 'Yogya';
/*
Nomor 13
*/
/*a*/ select * from tblpengarang where Nama like 'A%';
/*b*/ select * from tblpengarang where Nama like '%i';
/*c*/ select * from tblpengarang where Nama like '__a%'; 
/*d*/ select * from tblpengarang where Nama not like '%n';
/*
Nomor 14
*/
select * from tblpengarang as a
join tblgaji as b on a.Kd_Pengarang = b.Kd_Pengarang;
/*
Nomor 15
*/
select a.Kota from tblpengarang as a 
join tblgaji as b on a.Kd_Pengarang = b.Kd_Pengarang where b.Gaji < 1000000 group by a.Kota;
/*
Nomor 16
*/
ALTER TABLE tblpengarang
	CHANGE COLUMN Kelamin Kelamin VARCHAR(10) NOT NULL AFTER Kota;
/*
Nomor 17
*/
ALTER TABLE tblpengarang
	Add COLUMN Gelar VARCHAR(12) NOT NULL AFTER Nama;
/*
Nomor 18
*/
UPDATE tblpengarang SET Alamat ='Jl. Cendrawasih 65 ', Kota ='Pekanbaru' WHERE  Nama = 'Rian';
/*
Nomor 19
*/
CREATE VIEW `vwPengarang` AS SELECT Kd_Pengarang, Nama, Kota from tblpengarang ;