CREATE DATABASE dbkaryawan;
USE dbkaryawan;

CREATE TABLE `karyawan` (
	`id` BIGINT NOT NULL AUTO_INCREMENT,
	`nomor_induk` VARCHAR(7) NOT NULL,
	`nama` VARCHAR(30) NOT NULL,
	`alamat` TEXT NOT NULL,
	`tanggal_lahir` DATE NOT NULL,
	`tanggal_masuk` DATE NOT NULL,
	PRIMARY KEY (`nomor_induk``),
	
);

INSERT INTO `karyawan` (`id`, `nomor_induk`, `nama`, `alamat`, `tanggal_lahir`, `tanggal_masuk`) VALUES
	(1, 'IP06001', 'Agus', 'Jln. Gajah Mada 115A, Jakarta Pusat', '1970-08-01', '2006-07-07'),
	(2, 'IP06002', 'Amin', 'Jln. Bungur sari v No, 178, bandung', '1977-05-03', '2006-07-06'),
	(3, 'IP06003', 'Yusuf', 'Jln. Yosodpuro 15, surabaya', '1973-08-09', '2006-07-08'),
	(4, 'IP07004', 'Alyssa', 'Jln. Cendana No. 6 Bandung', '1983-02-14', '2007-01-05'),
	(5, 'IP07005', 'Maulana', 'Jln. Ampera Raya No 1', '1985-10-10', '2007-02-05'),
	(6, 'IP07006', 'Afika', 'Jln. Pejaten Barat No 6A', '1987-03-09', '2007-06-09'),
	(7, 'IP07007', 'James', 'Jln. Padjadjaran No. 111, bandung', '1988-05-19', '2006-06-09'),
	(8, 'IP09008', 'Octavanus', 'Jln. Gajah Mada 101, Semarang', '1988-10-07', '2008-08-08'),
	(9, 'IP09009', 'Nugroho', 'Jln. Duren Tiga 196, Jakarta Selatan', '1988-01-20', '2008-11-11'),
	(10, 'IP09010', 'Raisa', 'Jln. Nangka Jakarta selatan', '1989-12-29', '2009-02-09');
	
CREATE TABLE IF NOT EXISTS `cuti_karyawan` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `nomor_induk` VARCHAR(7) NOT NULL,
  `tanggal_mulai` DATE NOT NULL,
  `lama_cuti` INT(11) NOT NULL,
  `keterangan` TEXT NOT NULL,
  PRIMARY KEY (`id`)
  
);

INSERT INTO `cuti_karyawan` (`id`, `nomor_induk`, `tanggal_mulai`, `lama_cuti`, `keterangan`) VALUES
	(1, 'IP06001', '2012-02-01', 3, 'Acara keluar'),
	(2, 'IP06001', '2012-02-13', 4, 'Anak sakit'),
	(3, 'IP07007', '2012-02-15', 2, 'Nenek sakit'),
	(4, 'IP06003', '2012-02-17', 1, 'Mendaftar sekolah anak'),
	(5, 'IP07006', '2012-02-20', 5, 'Menikah'),
	(6, 'IP07004', '2012-02-27', 1, 'Imunisasi anak');
	
	
/*Nomor 1 Fix*/
SELECT * FROM karyawan WHERE (tanggal_masuk BETWEEN '2006-07-06' AND '2006-07-08') GROUP BY tanggal_masuk;
/*Nomor 2*/
SELECT b.nomor_induk, a.nama, b.tanggal_mulai, b.lama_cuti, b.keterangan 
FROM karyawan as a 
	JOIN cuti_karyawan as b 
	on a.nomor_induk = b.nomor_induk where (b.tanggal_mulai <= '2012-02-16' 
	and b.tanggal_mulai + b.lama_cuti > ('2012-02-16'));
/*Nomor 3 Fix*/
SELECT b.nomor_induk, a.nama, count(b.nomor_induk) as jumlah_cuti FROM karyawan as a 
RIGHT JOIN cuti_karyawan as b on a.nomor_induk = b.nomor_induk 
group by b.nomor_induk having count(b.nomor_induk) > 1;
/*Nomor 4*/
SELECT b.nomor_induk, a.nama, 12 - sum(b.lama_cuti) as sisa_cuti FROM karyawan as a 
LEFT JOIN cuti_karyawan as b on a.nomor_induk = b.nomor_induk group by a.nomor_induk, a.nama ;